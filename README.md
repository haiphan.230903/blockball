# BlockBall
This game was created for self-learning about game development with C++ by solo in 2 weeks when I was still in high school. My friends also helped me about game idea and some level designs. The game was inspired from Kirby Block Ball.

## Build
https://drive.google.com/file/d/1dm7Wyh3iBq60SmCwvlHt-tvgYeSkjuWS/view?usp=sharing

## Video trailer
https://www.youtube.com/watch?v=kmvjoAQo46E

## Video gameplay
https://youtu.be/1-Lcd7J1M4Q